DROP TABLE IF EXISTS salesman;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS customer;

CREATE TABLE salesman 
(
    salesman_id	varchar(300),
    name	varchar(300),
    city	varchar(300),
    commission	int
);

INSERT INTO salesman (salesman_id, name, city, commission) VALUES
	('5001', 'James Hoog', 'New York', '0.15'),
	('5002', 'Nail Knite', 'Paris', '0.13'),
	('5005', 'Pit Alex', 'London', '0.11'),
	('5006', 'Mc Lyon', 'Paris', '0.14'),
	('5007', 'Paul Adam', 'Rome', '0.13'),
	('5003', 'Lauson Hen', 'San Jose', '0.12');

CREATE TABLE orders
(
    ord_no	varchar(300),
    purch_amt	int,
    ord_date	varchar(300),
    customer_id	varchar(300),
    salesman_id	varchar(300)
);

INSERT INTO orders (ord_no, purch_amt, ord_date, customer_id, salesman_id) VALUES
	('70009', '270.65', '2012-09-10', '3001', '5005'),
	('70002', '65.26', '2012-10-05', '3002', '5001'),
	('70004', '110.50', '2012-08-17', '3009', '5003'),
	('70005', '2400.60', '2012-07-27', '3007', '5001'),
	('70008', '5760.00', '2012-09-10', '3002', '5001'),
	('70010', '1983.43', '2012-10-10', '3004', '5006'),
	('70003', '2480.40', '2012-10-10', '3009', '5003'),
	('70011', '75.29', '2012-08-17', '3003', '5007'),
	('70013', '3045.60', '2012-04-25', '3002', '5001'),
	('70001', '150.50', '2012-10-05', '3005', '5002'),
	('70007', '948.50', '2012-09-10', '3005', '5002'),
	('70012', '250.45', '2012-06-27', '3008', '5002');


CREATE TABLE customer
(
    customer_id	varchar(300),
    cust_name	varchar(300),
    city	varchar(300),
    grade	int,
    salesman_id	varchar(300)
);

INSERT INTO customer (customer_id, cust_name, city, grade, salesman_id) VALUES
	('3002', 'Nick Rimando', 'New York', '100', '5001'),
	('3007', 'Brad Davis', 'New York', '200', '5001'),
	('3005', 'Graham Zusi', 'California', '200', '5002'),
	('3008', 'Julian Green', 'London', '300', '5002'),
	('3004', 'Fabian Johnson', 'Paris', '300', '5006'),
	('3009', 'Geoff Cameron', 'Berlin', '100', '5003'),
	('3003', 'Jozy Altidor', 'Moscow', '200', '5007'),
	('3001', 'Brad Guzan', 'London', NULL, '5005');


